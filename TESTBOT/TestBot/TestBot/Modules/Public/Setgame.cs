﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Extensions.DependencyInjection;

namespace TestBot.Modules.Public
{
    public class Setgame : ModuleBase
    {
        [Command("SetGame")]
        [Summary("Sets a 'Game' for the bot")]
        public async Task Setgames([Remainder] string game)
        {
            if (!(Context.User.Id == 107213784279461888))
            {
                await Context.Channel.SendMessageAsync("Du kan ikke endre hva jeg spiller, snakk med Hanesor om du vil endre det");
            }
            else
            {
                await (Context.Client as DiscordSocketClient).SetGameAsync(game);
                await Context.Channel.SendMessageAsync($"Du har nå endret spiller jeg spiller til *{game}*");
                Console.WriteLine($"{DateTime.Now}: Game was changed to {game}");
            }
        }
    }
}
