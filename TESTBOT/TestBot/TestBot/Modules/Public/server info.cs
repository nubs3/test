﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using Discord.Webhook;
using System.Linq;
using Discord.Net;

namespace TestBot.Modules.Public
{
    public class Server_info : ModuleBase
    {
        [Command("Serverinfo")]
        public async Task Guildinfo()
        {
            EmbedBuilder embedbuilder;
            embedbuilder = new EmbedBuilder();
            embedbuilder.WithColor(new Color(0xbada55));

            var gld = Context.Guild as SocketGuild;
            var client = Context.Client as DiscordSocketClient;

            if (!string.IsNullOrWhiteSpace(gld.IconUrl))
                embedbuilder.ThumbnailUrl = gld.IconUrl;

            var O = gld.Owner.Username;
            var V = gld.VoiceRegionId;
            var C = gld.CreatedAt;
            var N = gld.DefaultMessageNotifications;
            var R = gld.Roles;
            var VL = gld.VerificationLevel;
            var XD = gld.Roles.Count;
            var X = gld.Roles.Count;
            var Z = client.ConnectionState;

            embedbuilder.Title = $"{gld.Name} Server Information";
            embedbuilder.Description = $"Server Owner: **{O}\n**Voice Region: **{V}\n**Created At: **{C}\n**Role Count: **{XD}\n **Members: **{X}\n **Conntection state: ** {Z}\n**";
            await ReplyAsync("", false, embedbuilder);
        }
    }
}
