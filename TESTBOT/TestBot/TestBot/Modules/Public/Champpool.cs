﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;

namespace TestBot.Modules.Public
{
    public class Champpool : ModuleBase
    {
        public string[] ALLRANDOM = new string[]
        {
            "Modules/Leaguebanner/AatroxBanner.png",
            "Modules/Leaguebanner/AhriBanner.png",
            "Modules/Leaguebanner/AkaliBanner.png",
            "Modules/Leaguebanner/CamilleBanner.png",
            "Modules/Leaguebanner/AlistarBanner.png",
            "Modules/Leaguebanner/AmumuBanner.png",
            "Modules/Leaguebanner/AniviaBanner.png",
            "Modules/Leaguebanner/AnnieBanner.png",
            "Modules/Leaguebanner/AsheBanner.png",
            "Modules/Leaguebanner/Aurelion_SolBanner.png",
            "Modules/Leaguebanner/AzirBanner.png",
            "Modules/Leaguebanner/BardBanner.png",
            "Modules/Leaguebanner/Blitzcrank.png",
            "Modules/Leaguebanner/BrandBanner.png",
            "Modules/Leaguebanner/BrandBanner.png",
            "Modules/Leaguebanner/CaitlynBanner.png",
            "Modules/Leaguebanner/CamilleBanner.png",
            "Modules/Leaguebanner/CassiopeiaBanner.png",
            "Modules/Leaguebanner/Cho'GathBanner.png",
            "Modules/Leaguebanner/CorkiBanner.png",
            "Modules/Leaguebanner/DariusBanner.png",
            "Modules/Leaguebanner/DianaBanner.png",
            "Modules/Leaguebanner/Dr._MundoBanner.png",
            "Modules/Leaguebanner/DravenBanner.png",
            "Modules/Leaguebanner/EkkoBanner.png",
            "Modules/Leaguebanner/EliseBanner.png",
            "Modules/Leaguebanner/EvelynnBanner.png",
            "Modules/Leaguebanner/EzrealBanner.png",
            "Modules/Leaguebanner/FiddlesticksBanner.png",
            "Modules/Leaguebanner/FioraBanner.png",
            "Modules/Leaguebanner/FizzBanner.png",
            "Modules/Leaguebanner/GalioBanner.png",
            "Modules/Leaguebanner/GangplankBanner.png",
            "Modules/Leaguebanner/GarenBanner.png",
            "Modules/Leaguebanner/GnarBanner.png",
            "Modules/Leaguebanner/GragasBanner.png",
            "Modules/Leaguebanner/GravesBanner.png",
            "Modules/Leaguebanner/HecarimBanner.png",
            "Modules/Leaguebanner/HeimerdingerBanner.png",
            "Modules/Leaguebanner/IllaoiBanner",
            "Modules/Leaguebanner/IreliaBanner.png",
            "Modules/Leaguebanner/IvernBanner.png",
            "Modules/Leaguebanner/JannaBanner.png",
            "Modules/Leaguebanner/Jarvan_IVBanner.png",
            "Modules/Leaguebanner/JaxBanner.png",
            "Modules/Leaguebanner/JayceBanner.png",
            "Modules/Leaguebanner/JhinBanner.png",
            "Modules/Leaguebanner/JinxBanner.png",
            "Modules/Leaguebanner/KalistaBanner.png",
            "Modules/Leaguebanner/KarmaBanner.png",
            "Modules/Leaguebanner/KarthusBanner.png",
            "Modules/Leaguebanner/KassadinBanner.png",
            "Modules/Leaguebanner/KatarinaBanner.png",
            "Modules/Leaguebanner/KayleBanner.png",
            "Modules/Leaguebanner/KaynBanner.png",
            "Modules/Leaguebanner/KennenBanner.png",
            "Modules/Leaguebanner/Kha'ZixBanner.png",
            "Modules/Leaguebanner/KindredBanner.png",
            "Modules/Leaguebanner/KledBanner.png",
            "Modules/Leaguebanner/Kog'MawBanner.png",
            "Modules/Leaguebanner/LeBlancBanner.png",
            "Modules/Leaguebanner/Lee_SinBanner.png",
            "Modules/Leaguebanner/LeonaBanner.png",
            "Modules/Leaguebanner/LissandraBanner.png",
            "Modules/Leaguebanner/LucianBanner.png",
            "Modules/Leaguebanner/LuluBanner.png",
            "Modules/Leaguebanner/LuxBanner.png",
            "Modules/Leaguebanner/MalphiteBanner.png",
            "Modules/Leaguebanner/MalzaharBanner.png",
            "Modules/Leaguebanner/MaokaiBanner.png",
            "Modules/Leaguebanner/Master_YiBanner.png",
            "Modules/Leaguebanner/Miss_FortuneBanner.png",
            "Modules/Leaguebanner/MordekaiserBanner.png",
            "Modules/Leaguebanner/MorganaBanner.png",
            "Modules/Leaguebanner/NamiBanner.png",
            "Modules/Leaguebanner/NasusBanner.png",
            "Modules/Leaguebanner/NautilusBanner.png",
            "Modules/Leaguebanner/NidaleeBanner.png",
            "Modules/Leaguebanner/NocturneBanner.png",
            "Modules/Leaguebanner/NunuBanner.png",
            "Modules/Leaguebanner/OlafBanner.png",
            "Modules/Leaguebanner/OriannaBanner.png",
            "Modules/Leaguebanner/OrnnBanner.png",
            "Modules/Leaguebanner/PantheonBanner.png",
            "Modules/Leaguebanner/PoppyBanner.png",
            "Modules/Leaguebanner/QuinnBanner.png",
            "Modules/Leaguebanner/RakanBanner.png",
            "Modules/Leaguebanner/RammusBanner.png",
            "Modules/Leaguebanner/Rek'SaiBanner.png",
            "Modules/Leaguebanner/RenektonBanner.png",
            "Modules/Leaguebanner/RengarBanner.png",
            "Modules/Leaguebanner/RivenBanner.png",
            "Modules/Leaguebanner/RumbleBanner.png",
            "Modules/Leaguebanner/RyzeBanner.png",
            "Modules/Leaguebanner/SejuaniBanner.png",
            "Modules/Leaguebanner/ShacoBanner.png",
            "Modules/Leaguebanner/ShenBanner.png",
            "Modules/Leaguebanner/ShyvanaBanner.png",
            "Modules/Leaguebanner/SingedBanner.png",
            "Modules/Leaguebanner/SionBanner.png",
            "Modules/Leaguebanner/SivirBanner.png",
            "Modules/Leaguebanner/SkarnerBanner.png",
            "Modules/Leaguebanner/SonaBanner.png",
            "Modules/Leaguebanner/SorakaBanner.png",
            "Modules/Leaguebanner/SwainBanner.png",
            "Modules/Leaguebanner/SyndraBanner.png",
            "Modules/Leaguebanner/Tham_KenchBanner.png",
            "Modules/Leaguebanner/TaliyahBanner.png",
            "Modules/Leaguebanner/TalonBanner.png",
            "Modules/Leaguebanner/TaricBanner.png",
            "Modules/Leaguebanner/TeemoBanner.png",
            "Modules/Leaguebanner/ThreshBanner.png",
            "Modules/Leaguebanner/TristanaBanner.png",
            "Modules/Leaguebanner/TrundleBanner.png",
            "Modules/Leaguebanner/TryndamereBanner.png",
            "Modules/Leaguebanner/Twisted_FateBanner.png",
            "Modules/Leaguebanner/TwitchBanner.png",
            "Modules/Leaguebanner/UdyrBanner.png",
            "Modules/Leaguebanner/UrgotBanner.png",
            "Modules/Leaguebanner/VarusBanner.png",
            "Modules/Leaguebanner/VayneBanner.png",
            "Modules/Leaguebanner/VeigarBanner.png",
            "Modules/Leaguebanner/Vel'KozBanner.png",
            "Modules/Leaguebanner/ViBanner.png",
            "Modules/Leaguebanner/ViktorBanner.png",
            "Modules/Leaguebanner/VladimirBanner.png",
            "Modules/Leaguebanner/VolibearBanner.png",
            "Modules/Leaguebanner/WarwickBanner.png",
            "Modules/Leaguebanner/WukongBanner.png",
            "Modules/Leaguebanner/XayahBanner.png",
            "Modules/Leaguebanner/XerathBanner.png",
            "Modules/Leaguebanner/Xin_ZhaoBanner.png",
            "Modules/Leaguebanner/YasuoBanner.png",
            "Modules/Leaguebanner/YorickBanner.png",
            "Modules/Leaguebanner/ZacBanner.png",
            "Modules/Leaguebanner/ZedBanner.png",
            "Modules/Leaguebanner/ZiggsBanner.png",
            "Modules/Leaguebanner/ZileanBanner.png",
            //"Modules/Leaguebanner/ZoeBanner.png",
            "Modules/Leaguebanner/ZyraBanner.png"
        };

        public string[] TOP = new string[]
        {
            "Modules/Leaguebanner/AatroxBanner.png",
            "Modules/Leaguebanner/AkaliBanner.png",
            "Modules/Leaguebanner/CamilleBanner.png",
            "Modules/Leaguebanner/Cho'GathBanner.png",
            "Modules/Leaguebanner/DariusBanner.png",
            "Modules/Leaguebanner/Dr._MundoBanner.png",
            "Modules/Leaguebanner/EkkoBanner.png",
            "Modules/Leaguebanner/FioraBanner.png",
            "Modules/Leaguebanner/FizzBanner.png",
            "Modules/Leaguebanner/GalioBanner.png",
            "Modules/Leaguebanner/GangplankBanner.png",
            "Modules/Leaguebanner/GarenBanner.png",
            "Modules/Leaguebanner/GnarBanner.png",
            "Modules/Leaguebanner/HeimerdingerBanner.png",
            "Modules/Leaguebanner/IllaoiBanner",
            "Modules/Leaguebanner/IreliaBanner.png",
            "Modules/Leaguebanner/Jarvan_IVBanner.png",
            "Modules/Leaguebanner/JaxBanner.png",
            "Modules/Leaguebanner/JayceBanner.png",
            "Modules/Leaguebanner/KayleBanner.png",
            "Modules/Leaguebanner/KennenBanner.png",
            "Modules/Leaguebanner/KledBanner.png",
            "Modules/Leaguebanner/MalphiteBanner.png",
            "Modules/Leaguebanner/MordekaiserBanner.png",
            "Modules/Leaguebanner/NasusBanner.png",
            "Modules/Leaguebanner/NautilusBanner.png",
            "Modules/Leaguebanner/NunuBanner.png",
            "Modules/Leaguebanner/OlafBanner.png",
            "Modules/Leaguebanner/OrnnBanner.png",
            "Modules/Leaguebanner/PantheonBanner.png",
            "Modules/Leaguebanner/PoppyBanner.png",
            "Modules/Leaguebanner/QuinnBanner.png",
            "Modules/Leaguebanner/RammusBanner.png",
            "Modules/Leaguebanner/Rek'SaiBanner.png",
            "Modules/Leaguebanner/RenektonBanner.png",
            "Modules/Leaguebanner/RivenBanner.png",
            "Modules/Leaguebanner/RumbleBanner.png",
            "Modules/Leaguebanner/ShenBanner.png",
            "Modules/Leaguebanner/ShyvaneBanner.png",
            "Modules/Leaguebanner/SingedBanner.png",
            "Modules/Leaguebanner/SionBanner.png",
            "Modules/Leaguebanner/SwainBanner.png",
            "Modules/Leaguebanner/Tham_KenchBanner.png",
            "Modules/Leaguebanner/TeemoBanner.png",
            "Modules/Leaguebanner/TrundleBanner.png",
            "Modules/Leaguebanner/TryndamereBanner.png",
            "Modules/Leaguebanner/UdyrBanner.png",
            "Modules/Leaguebanner/UrgotBanner.png",
            "Modules/Leaguebanner/VladimirBanner.png",
            "Modules/Leaguebanner/VolibearBanner.png",
            "Modules/Leaguebanner/WukongBanner.png",
            "Modules/Leaguebanner/Xin_ZhaoBanner.png",
            "Modules/Leaguebanner/YasuoBanner.png",
            "Modules/Leaguebanner/YorickBanner.png"
        };

        public string[] JUNGLE = new string[]
        {
            "Modules/Leaguebanner/AkaliBanner.png",
            "Modules/Leaguebanner/AmumuBanner.png",
            "Modules/Leaguebanner/CamilleBanner.png",
            "Modules/Leaguebanner/Cho'GathBanner.png",
            "Modules/Leaguebanner/DianaBanner.png",
            "Modules/Leaguebanner/EkkoBanner.png",
            "Modules/Leaguebanner/EliseBanner.png",
            "Modules/Leaguebanner/EvelynnBanner.png",
            "Modules/Leaguebanner/FiddlesticksBanner.png",
            "Modules/Leaguebanner/FizzBanner.png",
            "Modules/Leaguebanner/GragasBanner.png",
            "Modules/Leaguebanner/HecaarimBanner.png",
            "Modules/Leaguebanner/IreliaBanner.png",
            "Modules/Leaguebanner/IvernBanner.png",
            "Modules/Leaguebanner/Jarvan_IVBanner.png",
            "Modules/Leaguebanner/JaxBanner.png",
            "Modules/Leaguebanner/KaynBanner.png",
            "Modules/Leaguebanner/Kha'zixBanner.png",
            "Modules/Leaguebanner/KindredBanner.png",
            "Modules/Leaguebanner/Lee_SinBanner.png",
            "Modules/Leaguebanner/MaokaiBanner.png",
            "Modules/Leaguebanner/Master_YiBanner.png",
            "Modules/Leaguebanner/NautilusBanner.png",
            "Modules/Leaguebanner/NidaleeBanner.png",
            "Modules/Leaguebanner/NocturneBanner.png",
            "Modules/Leaguebanner/NunuBanner.png",
            "Modules/Leaguebanner/RammusBanner.png",
            "Modules/Leaguebanner/Rek'SaiBanner.png",
            "Modules/Leaguebanner/RengarBanner.png",
            "Modules/Leaguebanner/SejuaniBanner.png",
            "Modules/Leaguebanner/ShacoBanner.png",
            "Modules/Leaguebanner/ShyvanaBanner.png",
            "Modules/Leaguebanner/SkarnerBanner.png",
            "Modules/Leaguebanner/UdyrBanner.png",
            "Modules/Leaguebanner/ViBanner.png",
            "Modules/Leaguebanner/VolibearBanner.png",
            "Modules/Leaguebanner/WarwickBanner.png",
            "Modules/Leaguebanner/WukongBanner.png",
            "Modules/Leaguebanner/Xin_ZhaoBanner.png",
            "Modules/Leaguebanner/ZacBanner.png"
        };

        public string[] MID = new string[]
        {
            "Modules/Leaguebanner/AhirBanner.png",
            "Modules/Leaguebanner/AkaliBanner.png",
            "Modules/Leaguebanner/AniviaBanner.png",
            "Modules/Leaguebanner/AnnieBanner.png",
            "Modules/Leaguebanner/Aurelion_SolBanner.png",
            "Modules/Leaguebanner/AzirBanner.png",
            "Modules/Leaguebanner/BrandBanner.png",
            "Modules/Leaguebanner/CassiopeiaBanner.png",
            "Modules/Leaguebanner/EkkoBanner.png",
            "Modules/Leaguebanner/FizzBanner.png",
            "Modules/Leaguebanner/GragasBanner.png",
            "Modules/Leaguebanner/HeimerdingerBanner.png",
            "Modules/Leaguebanner/KarmaBanner.png",
            "Modules/Leaguebanner/KarthusBanner.png",
            "Modules/Leaguebanner/KassadinBanner.png",
            "Modules/Leaguebanner/KatarinaBanner.png",
            "Modules/Leaguebanner/KayleBanner.png",
            "Modules/Leaguebanner/LeBlancBanner.png",
            "Modules/Leaguebanner/LissandraBanner.png",
            "Modules/Leaguebanner/LuxBanner.png",
            "Modules/Leaguebanner/MalzaharBanner.png",
            "Modules/Leaguebanner/MorganaBanner.png",
            "Modules/Leaguebanner/OriannaBanner.png",
            "Modules/Leaguebanner/RyzeBanner.png",
            "Modules/Leaguebanner/SwainBanner.png",
            "Modules/Leaguebanner/SyndraBanner.png",
            "Modules/Leaguebanner/TaliyahBanner.png",
            "Modules/Leaguebanner/TalonBanner.png",
            "Modules/Leaguebanner/Twisted_FateBanner.png",
            "Modules/Leaguebanner/VeigarBanner.png",
            "Modules/Leaguebanner/Vel'KozBanner.png",
            "Modules/Leaguebanner/ViktorBanner.png",
            "Modules/Leaguebanner/VladimirBanner.png",
            "Modules/Leaguebanner/XerathBanner.png",
            "Modules/Leaguebanner/YasuoBanner.png",
            "Modules/Leaguebanner/ZedBanner.png",
            "Modules/Leaguebanner/ZiggsBanner.png",
            "Modules/Leaguebanner/ZileanBanner.png",
            //"Modules/Leaguebanner/ZoeBanner.png",
            "Modules/Leaguebanner/ZyraBanner.png"
        };

        public string[] ADC = new string[]
        {
            "Modules/Leaguebanner/AsheBanner.png",
            "Modules/Leaguebanner/CaitlynBanner.png",
            "Modules/Leaguebanner/CorkiBanner.png",
            "Modules/Leaguebanner/DravenBanner.png",
            "Modules/Leaguebanner/EzrealBanner.png",
            "Modules/Leaguebanner/GravesBanner.png",
            "Modules/Leaguebanner/JhinBanner.png",
            "Modules/Leaguebanner/JinxBanner.png",
            "Modules/Leaguebanner/KalistaBanner.png",
            "Modules/Leaguebanner/KindredBanner.png",
            "Modules/Leaguebanner/Kog'MawBanner.png",
            "Modules/Leaguebanner/LucianBanner.png",
            "Modules/Leaguebanner/Miss_FortuneBanner.png",
            "Modules/Leaguebanner/QuinnBanner.png",
            "Modules/Leaguebanner/SivirBanner.png",
            "Modules/Leaguebanner/TeemoBanner.png",
            "Modules/Leaguebanner/TristanaBanner.png",
            "Modules/Leaguebanner/TwitchBanner.png",
            "Modules/Leaguebanner/VarusBanner.png",
            "Modules/Leaguebanner/VayneBanner.png",
            "Modules/Leaguebanner/XayahBanner.png"
        };

        public string[] SUPPORT = new string[]
        {
            "Modules/Leaguebanner/AlistarBanner.png",
            "Modules/Leaguebanner/BardBanner.png",
            "Modules/Leaguebanner/BlitzcrankBanner.png",
            "Modules/Leaguebanner/BraumBanner.png",
            "Modules/Leaguebanner/GalioBanner.png",
            "Modules/Leaguebanner/IvernBanner.png",
            "Modules/Leaguebanner/JannaBanner.png",
            "Modules/Leaguebanner/KarmaBanner.png",
            "Modules/Leaguebanner/LeonaBanner.png",
            "Modules/Leaguebanner/LuluBanner.png",
            "Modules/Leaguebanner/MorganaBanner.png",
            "Modules/Leaguebanner/NamiBanner.png",
            "Modules/Leaguebanner/NautilusBanner.png",
            "Modules/Leaguebanner/PoppyBanner.png",
            "Modules/Leaguebanner/RakanBanner.png",
            "Modules/Leaguebanner/ShenBanner.png",
            "Modules/Leaguebanner/SorakaBanner.png",
            "Modules/Leaguebanner/Tham_KenchBanner.png",
            "Modules/Leaguebanner/TaricBanner.png",
            "Modules/Leaguebanner/ThreshBanner.png",
            "Modules/Leaguebanner/ZileanBanner.png",
            "Modules/Leaguebanner/ZyraBanner.png"
        };
    
        Random rand = new Random();

        [Command("random top")]
        public async Task Top()
        {
            int randomtop = rand.Next(TOP.Length);
            string GIVETOP = TOP[randomtop];
            await Context.Channel.SendFileAsync(GIVETOP);
        }

        [Command("random jungle")]
        public async Task Jungle()
        {
            int randomjungle = rand.Next(JUNGLE.Length);
            string GIVEJUNGLE = JUNGLE[randomjungle];
            await Context.Channel.SendFileAsync(GIVEJUNGLE);
        }

        [Command("random mid")]
        public async Task Mid()
        {
            int randommid = rand.Next(MID.Length);
            string GIVEMID = MID[randommid];
            await Context.Channel.SendFileAsync(GIVEMID);
        }

        [Command("random adc")]
        public async Task Adc()
        {
            int randomadc = rand.Next(ADC.Length);
            string GIVEADC = ADC[randomadc];
            await Context.Channel.SendFileAsync(GIVEADC);
        }

        [Command("random support")]
        public async Task Support()
        {
            int randomsupport = rand.Next(SUPPORT.Length);
            string GIVESUPPORT = SUPPORT[randomsupport];
            await Context.Channel.SendFileAsync(GIVESUPPORT);
        }

        [Command("teamcomp")]
        public async Task Teamcomp()
        {
            int randomtop = rand.Next(TOP.Length);
            int randomjungle = rand.Next(JUNGLE.Length);
            int randommid = rand.Next(MID.Length);
            int randomadc = rand.Next(ADC.Length);
            int randomsupport = rand.Next(SUPPORT.Length);

            string GIVECOMP1 = TOP[randomtop];
            string GIVECOMP2 = JUNGLE[randomjungle];
            string GIVECOMP3 = MID[randommid];
            string GIVECOMP4 = ADC[randomadc];
            string GIVECOMP5 = SUPPORT[randomsupport];

            await Context.Channel.SendFileAsync(GIVECOMP1);
            await Context.Channel.SendFileAsync(GIVECOMP2);
            await Context.Channel.SendFileAsync(GIVECOMP3);
            await Context.Channel.SendFileAsync(GIVECOMP4);
            await Context.Channel.SendFileAsync(GIVECOMP5);
        }

        [Command("all random")]
        public async Task Allrandom()
        {
            int randomall1 = rand.Next(ALLRANDOM.Length);
            string GIVEALLRANDOM1 = ALLRANDOM[randomall1];

            await Context.Channel.SendFileAsync(GIVEALLRANDOM1);

            int randomall2 = rand.Next(ALLRANDOM.Length);
            string GIVEALLRANDOM2 = ALLRANDOM[randomall2];

            await Context.Channel.SendFileAsync(GIVEALLRANDOM2);

            int randomall3 = rand.Next(ALLRANDOM.Length);
            string GIVEALLRANDOM3 = ALLRANDOM[randomall3];

            await Context.Channel.SendFileAsync(GIVEALLRANDOM3);

            int randomall4 = rand.Next(ALLRANDOM.Length);
            string GIVEALLRANDOM4 = ALLRANDOM[randomall4];

            await Context.Channel.SendFileAsync(GIVEALLRANDOM4);

            int randomall5 = rand.Next(ALLRANDOM.Length);
            string GIVEALLRANDOM5 = ALLRANDOM[randomall5];

            await Context.Channel.SendFileAsync(GIVEALLRANDOM5);
        }
    }
}
