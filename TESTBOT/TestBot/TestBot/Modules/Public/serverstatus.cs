﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;

namespace TestBot.Modules.Public
{
    public class MinecraftCommands : ModuleBase
    {
        public class MinecraftServerInfo
        {
            [JsonProperty("status")]
            public string Status { get; set; }

            [JsonProperty("online")]
            public bool IsOnline { get; set; }

        }

        [Command("minecraft")]
        public async Task MinecraftStatus()
        {
            MinecraftServerInfo Info;
            var httpClient = new HttpClient();
            string URL = "https://mcapi.us/server/status?ip=ehhei.hopto.org"; //Change play.enixgaming.com to the ip of your choice
            var response = await httpClient.GetAsync(URL);
            var result = await response.Content.ReadAsStringAsync();
            Info = JsonConvert.DeserializeObject<MinecraftServerInfo>(result);
            if (Info.Status == "success")
            {
                if (Info.IsOnline)
                    await Context.Channel.SendMessageAsync($"The server is online");
                else
                    await Context.Channel.SendMessageAsync($"The server is offline");
            }
        }
    }
}
