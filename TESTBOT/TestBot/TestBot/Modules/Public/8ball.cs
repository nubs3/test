﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;

namespace MyBot.Modules.Public
{
    public class _8ball : ModuleBase
    {
       string[] predictionsText = new string[]
        {
        "ja",
        "nei"
        };

        Random rand = new Random();
        [Command("8ball")]
        [Summary("gives a prediction")]
        public async Task Eightball([Remainder] string input)
        {
            int randomIndex = rand.Next(predictionsText.Length);
            string text = predictionsText[randomIndex];
            await ReplyAsync(Context.User.Mention + ", " + text);
        }
    }

}
