﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using Discord.Webhook;

namespace TestBot.Modules.Public
{
    public class KickandBan : ModuleBase
    {
        [Command("Ban")]
        [RequireUserPermission(GuildPermission.BanMembers)]
        [RequireBotPermission(GuildPermission.BanMembers)]
        public async Task BanAsync(SocketGuildUser user = null, [Remainder] string reason = null)
        {
            if (user == null) throw new ArgumentException("You must mention a user");
            if (string.IsNullOrWhiteSpace(reason)) throw new ArgumentException("You must provide a reason");

            var gld = Context.Guild as SocketGuild;
            var embed = new EmbedBuilder();
            embed.WithColor(new Color(0xff0000));
            embed.Title = $"**{user.Username}** was banned";
            embed.Description = $"**{user.Username}\n**Guild Name: **{user.Guild.Name}\n**Banned by: **{Context.User.Mention}!\n Reason: **{reason}";

            await gld.AddBanAsync(user);
            await Context.Channel.SendMessageAsync("", false, embed);           
        }

       
        [Command("Kick")]
        [RequireBotPermission(GuildPermission.KickMembers)]
        [RequireUserPermission(GuildPermission.KickMembers)]
        public async Task KickAsync(SocketGuildUser user = null, [Remainder] string reason = null)
        {
            if (user == null) throw new ArgumentException("You must mention a user");
            if (string.IsNullOrWhiteSpace(reason)) throw new ArgumentException("you must provide a reason");

            var gld = Context.Guild as SocketGuild;
            var embed = new EmbedBuilder();
            embed.WithColor(new Color(0xffa500));
            embed.Title = $" {user.Username} has been kicked from {user.Guild.Name}";
            embed.Description = $"**Username: **{user.Username}\n**Guild Name: **{user.Guild.Name}\n**Kicked by: **{Context.User.Mention}!\n**Reason: **{reason}";

            await user.KickAsync();
            await Context.Channel.SendMessageAsync("", false, embed);
        }

    }
}
