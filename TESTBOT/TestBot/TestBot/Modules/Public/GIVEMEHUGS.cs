﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;

namespace ConsoleApp1.Modules.Public
{
    public class GIVEMEHUGS : ModuleBase
    {
        public string[] GIVEHUGS = new string[]
        {
        "Modules/HUGS/Hug.gif",
        "Modules/HUGS/Hug2.gif",
        "Modules/HUGS/Hug3.gif",
        "Modules/HUGS/Hug4.gif",
        "Modules/HUGS/Hug5.gif",
        "Modules/HUGS/Hug6.gif",
        "Modules/HUGS/Hug7.gif",
        "Modules/HUGS/Hug8.gif",
        "Modules/HUGS/Hug9.gif",
        "Modules/HUGS/Hug10.gif",
        "Modules/HUGS/Hug11.gif",
        "Modules/HUGS/Hug12.gif",
        "Modules/HUGS/Hug13.gif",
        "Modules/HUGS/Hug14.gif",
        "Modules/HUGS/Hug15.gif",
        "Modules/HUGS/Hug16.gif",
        "Modules/HUGS/Hug17.gif",
        "Modules/HUGS/Hug18.gif",
        "Modules/HUGS/Hug19.gif",
        "Modules/HUGS/Hug20.gif",
        "Modules/HUGS/Hug22.gif",
        "Modules/HUGS/Hug23.gif",
        "Modules/HUGS/Hug24.gif",
        "Modules/HUGS/Hug25.gif",
        "Modules/HUGS/Hug26.gif",
        "Modules/HUGS/Hug27.gif",
        "Modules/HUGS/Hug28.gif",
        "Modules/HUGS/Hug29.gif",
        "Modules/HUGS/Hug30.gif",
        "Modules/HUGS/Hug31.gif",
        "Modules/HUGS/Hug32.gif",
        "Modules/HUGS/Hug33.gif",
        "Modules/HUGS/Hug34.gif",
        "Modules/HUGS/Hug35.gif",
        "Modules/HUGS/Hug36.gif",
        "Modules/HUGS/Hug37.gif",
        "Modules/HUGS/Hug38.gif",
        "Modules/HUGS/Hug39.gif",
        "Modules/HUGS/Hug40.gif",
        "Modules/HUGS/Hug41.gif",
        "Modules/HUGS/Hug42.gif",
        "Modules/HUGS/Hug43.gif",
        "Modules/HUGS/Hug44.gif",
        "Modules/HUGS/Hug45.gif",
        "Modules/HUGS/Hug46.gif",
        "Modules/HUGS/Hug47.gif",
        "Modules/HUGS/Hug48.gif",
        "Modules/HUGS/Hug49.gif",
        "Modules/HUGS/Hug50.gif",
        "Modules/HUGS/Hug51.gif"
        };
        Random rand = new Random();



        [Command("Hug")]
        public async Task HUGME(IGuildUser user)
        {
            int randomgif = rand.Next(GIVEHUGS.Length);
            string Gifs = GIVEHUGS[randomgif];
            await Context.Channel.SendFileAsync(Gifs, Context.User.Mention + " Hugs you, " + user.Mention);
        }

    }
}
