﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using Discord.Webhook;
using System.Text;
using System.Linq;

namespace TestBot.Modules.Public
{
    public class Botinfo : ModuleBase
    {
        [Command("botinfo")]
        [Summary("Shows All Bot Info.")]
        public async Task Info()
        {
            using (var process = Process.GetCurrentProcess())
            {
                var embed = new EmbedBuilder();
                var application = await Context.Client.GetApplicationInfoAsync();
                embed.ImageUrl = application.IconUrl;
                embed.WithColor(new Color(0x02fc1f))

                .AddField(y =>
                {
                    y.Name = "Author.";
                    y.Value = application.Owner.Username; application.Owner.Id.ToString();
                    y.IsInline = false;
                })
                .AddField(y =>
                {
                    y.Name = "Uptime.";
                    var time = DateTime.Now - process.StartTime;
                    var sb = new StringBuilder();
                    if (time.Days > 0)
                    {
                        sb.Append($"{time.Days}d ");
                    }
                    if (time.Hours > 0)
                    {
                        sb.Append($"{time.Hours}h ");
                    }
                    if (time.Minutes > 0)
                    {
                        sb.Append($"{time.Minutes}m ");
                    }
                    sb.Append($"{time.Seconds}s ");
                    y.Value = sb.ToString();
                    y.IsInline = true;
                })
                .AddField(y =>
                {
                    y.Name = "Discord.net version.";
                    y.Value = DiscordConfig.Version;
                    y.IsInline = true;
                }).AddField(y =>
                {
                    y.Name = "Server Amount.";
                    y.Value = (Context.Client as DiscordSocketClient).Guilds.Count.ToString();
                    y.IsInline = false;
                })
                .AddField(y =>
                {
                    y.Name = "Heap Size";
                    y.Value = GetHeapSize();
                    y.IsInline = false;
                })
                .AddField(y =>
                {
                    y.Name = "Number Of Users";
                    y.Value = (Context.Client as DiscordSocketClient).Guilds.Sum(g => g.Users.Count).ToString();
                    y.IsInline = false;
                })
                .AddField(y =>
                {
                    y.Name = "Channels";
                    y.Value = (Context.Client as DiscordSocketClient).Guilds.Sum(g => g.Channels.Count).ToString();
                    y.IsInline = false;
                });
                await this.ReplyAsync("", embed: embed);
            }
        }
        private static string Getuptime()
            => (DateTime.Now - Process.GetCurrentProcess().StartTime).ToString(@"dd\.hh\:mm\:ss");
        private static string GetHeapSize() => Math.Round(GC.GetTotalMemory(true) / (1024.0 * 1024.0), 2).ToString();
    }
}
