﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using Discord.Webhook;
using System.Linq;

namespace TestBot.Modules.Public
{
    public class CountUsers : ModuleBase
    {
        [Command("countusers")]
        [Alias("count", "users")]
        public async Task Countthem()
        {
            using (var process = Process.GetCurrentProcess())
            {
                var embed = new EmbedBuilder();
                var application = await Context.Client.GetApplicationInfoAsync();
                embed.ImageUrl = application.IconUrl;
                embed.WithColor(new Color(0x00fffff))

                    .AddField(y =>
                    {
                        y.Name = "number of Users";
                        y.Value = (Context.Client as DiscordSocketClient).Guilds.Sum(g => g.Users.Count).ToString();
                        y.IsInline = false;
                    });
                await this.ReplyAsync("", embed: embed);
            }
        }
    }
}
