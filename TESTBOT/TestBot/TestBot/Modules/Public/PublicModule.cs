﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;

namespace MyBot.Modules.Public
{

    public class PublicModule : ModuleBase
    {
        [Command("Ping")]
        [Alias("ping")]
        [Summary("replies Pong!")]
        public async Task Ping()
        {
            await ReplyAsync("Pong!");
        }
    }
}