﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using Discord.Webhook;
using System.Linq;
using Discord.Net;
using Discord.Audio;
using System.Collections.Concurrent;

namespace TestBot.Modules.Public
{
    public class Music : ModuleBase<ICommandContext>
    {
        private readonly AudioService _service;

        public Process CreateStream(string url)
        {
#pragma warning disable IDE0017 // Simplify object initialization
            Process currentsong = new Process();
#pragma warning restore IDE0017 // Simplify object initialization

            currentsong.StartInfo = new ProcessStartInfo
            {
                FileName = "cmd.exe",
                Arguments = $"/C youtube-dl.exe -o - {url} | ffmpeg -i pipe:0 -ac 2 -f s16le -ar 48000 pipe:1",
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            };

            currentsong.Start();
            return currentsong;
        }

        public class AudioService
        {
            private readonly ConcurrentDictionary<ulong, IAudioClient> ConnectedChannels = new ConcurrentDictionary<ulong, IAudioClient>();

            public async Task LeaveAudio(IGuild guild)
            {
#pragma warning disable IDE0018 // Inline variable declaration
                IAudioClient client;
#pragma warning restore IDE0018 // Inline variable declaration
                if (ConnectedChannels.TryRemove(guild.Id, out client))
                {
                    await client.StopAsync();
                }
            }
        }

        [Command("play", RunMode = RunMode.Async)]
        public async Task Play(string url)
        {
            IVoiceChannel channel = (Context.User as IVoiceState).VoiceChannel;
            IAudioClient client = await channel.ConnectAsync();

            var output = CreateStream(url).StandardOutput.BaseStream;
            var stream = client.CreatePCMStream(AudioApplication.Music, 128 * 1024);
            await output.CopyToAsync(stream);
            await stream.FlushAsync().ConfigureAwait(false);
        }

        [Command("leave", RunMode = RunMode.Async)]
        public async Task Leaveme()
        {
            IVoiceChannel channel = (Context.User as IVoiceState).VoiceChannel;
            IAudioClient client = await channel.ConnectAsync();
            await _service.LeaveAudio(Context.Guild);
        }
    }
}
