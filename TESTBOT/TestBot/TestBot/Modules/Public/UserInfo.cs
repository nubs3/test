﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using Discord.Webhook;
using System.Linq;
using Discord.Net;

namespace TestBot.Modules.Public
{
    public class UserInfo : ModuleBase
    {
        [Command("userinfo")]
        [Alias("user")]
        public async Task GetUserInfo(IGuildUser user)
        {
            var application = await Context.Client.GetApplicationInfoAsync();
            var date = $"{user.CreatedAt.Day}/{user.CreatedAt.Month}/{user.CreatedAt.Year}";
            var auth = new EmbedAuthorBuilder();

            var embed = new EmbedBuilder()

            {
                Color = new Color(29, 140, 209),
                Author = auth
            };

            var us = user as SocketGuildUser;

            var D = us.Username;

            var A = us.Discriminator;
            var T = us.Id;
            var S = date;
            var C = us.Status;
            var CC = us.JoinedAt;
            var O = us.Game;
            embed.Title = $"**{us.Username}** Information";
            embed.Description = $"Username: **{D}**\nDiscriminator: **{A}**\nUser ID: **{T}**\nCreated at: **{S}**\nCurrent Status: **{C}**\nJoined server at: **{CC}**\nPlaying: **{O}**";

            await ReplyAsync("", false, embed.Build());
        }
    }
}
