﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using Discord.Webhook;

namespace MyBot
{
    public class Program
    {
        private CommandService commands;
        private DiscordSocketClient client;
        private IServiceProvider _provider;

        static public void Main(string[] args) => new Program().Start().GetAwaiter().GetResult();

        public async Task Start()
        { 
            client = new DiscordSocketClient();
            commands = new CommandService();

            client.Log += Log;
            string token = "MjkzNjkwMDc2Njc0OTE2MzUy.C_RtdQ.dHZq-SuTW87U1ElhbEmWl3wQE4s";


            await InstallCommands();


            await client.LoginAsync(TokenType.Bot, token);
            await client.StartAsync();
            await Task.Delay(-1);


        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        private async Task InstallCommands()
        {
            client.MessageReceived += HandleCommand;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly());

            client.Ready += setdagame;
            async Task setdagame()
            {
                await Task.Delay(1000);
                await client.SetGameAsync("against its creator");
                Console.WriteLine($"{DateTime.Now}: Game was changed to against its creator");
            }
        }

        public async Task HandleCommand(SocketMessage messageParam)
        {
            var message = messageParam as SocketUserMessage;

            if (message == null) return;

            int argPos = 1;

            if (!(message.HasStringPrefix("&", ref argPos) || message.HasMentionPrefix(client.CurrentUser, ref argPos))) return;

            var context = new CommandContext(client, message);

            var result = await commands.ExecuteAsync(context, argPos, _provider);

            if (!result.IsSuccess)
                await context.Channel.SendMessageAsync(result.ErrorReason);

          


        }

       
        private IServiceProvider ConfigureServices()
        {
            var services = new ServiceCollection()
                .AddSingleton(client)
                //.AddSingleton<AudioService>()
                .AddSingleton(new CommandService(new CommandServiceConfig { CaseSensitiveCommands = false }));

            var provider = new DefaultServiceProviderFactory().CreateServiceProvider(services);
            //provider.GetService<AudioService>();

            return provider;
        }

        
        
    }

}